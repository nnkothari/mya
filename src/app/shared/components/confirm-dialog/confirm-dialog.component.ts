import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs/internal/Subject';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {
  public onClose: Subject<boolean>;
  constructor(private modalRef: BsModalRef) { }

  ngOnInit() {
    this.onClose = new Subject();
  }

  confirm(): void {
    this.onClose.next(true);
    this.modalRef.hide();
  }

  decline(): void {
    this.onClose.next(false);
    this.modalRef.hide();
  }

}
