import { Component, OnInit } from '@angular/core';
import { TranslationsService } from 'src/app/core/services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  selectedLanguage = 'en';
  languages = [
    {
      id: 'en',
      name: 'En'
    },
    {
      id: 'fr',
      name: 'Fr'
    },
  ];
  selectedColor = '#0f5cd5';
  constructor(private translationsService: TranslationsService) { }

  ngOnInit() {
  }

  changeLanguage(language: string) {
    this.translationsService.changeLanguage(language);
  }

  //Theme Color  
  changeTheme(primary: string) {
    document.documentElement.style.setProperty('--primary', primary); 
  }

}
