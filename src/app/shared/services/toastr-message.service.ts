import { Injectable } from '@angular/core';
import { ToastrService, GlobalConfig } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})

export class Message {
  title: string;
  text: string;
  type: string;
}
export class ToastrMessageService {
  constructor(private toastr: ToastrService) {
  }

  showMessage(message: Message) {
    switch (message.type) {
      case 'success':
        this.toastr.success(message.text, message.title, {});
        break;
      case 'error':
        this.toastr.error(message.text, message.title, {});
        break;
      case 'info':
        this.toastr.info(message.text, message.title, {});
        break;
      case 'warning':
        this.toastr.warning(message.text, message.title, {});
        break;
    }
  }
}
