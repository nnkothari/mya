import { Injectable, Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Observable, of, Subject } from 'rxjs';
import { ConfirmDialogComponent } from '../components/confirm-dialog/confirm-dialog.component';
import { InfoDialogComponent } from '../components/info-dialog/info-dialog.component';


@Injectable({
  providedIn: 'root'
})
export class SharedService {
  bsModalRef: BsModalRef;
  constructor(private modalService: BsModalService) { }

  openModalPopup(type: string, data: {}, component?: Component): Observable<boolean> {
    const initialState = { title: 'Confirm', message: 'Confirm ??' };
    switch (type) {
      case 'confirmDialog':
        this.bsModalRef = this.modalService.show(ConfirmDialogComponent, { initialState });
        return this.bsModalRef.content.onClose;
        break;
      case 'infoDialog':
        this.bsModalRef = this.modalService.show(InfoDialogComponent, data);
        break;
      case 'custom':
        this.bsModalRef = this.modalService.show(component, data);
        return this.bsModalRef.content.onClose;
        break;
    }
  }
}
