import { FormControl } from '@angular/forms';
import { Regex } from '../models/regex';

export class Validation {
    static isValidMailFormat(control: FormControl) {
        const EMAIL_REGEXP = Regex.email;
        if (control.value && !EMAIL_REGEXP.test(control.value)) {
            return { inValidEmail: true };
        }
    }
}
