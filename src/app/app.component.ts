import { Component } from '@angular/core';
import * as AOS from 'aos';
import cssVars from 'css-vars-ponyfill';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'angular-structure';
  
  
  ngOnInit(){
    //Animation
    AOS.init();
    //Color theme
    cssVars({
      variables: {
          '--primary': '#0f5cd5',        
      }
    });
  }
}
