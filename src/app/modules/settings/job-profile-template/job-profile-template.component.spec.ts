import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobProfileTemplateComponent } from './job-profile-template.component';

describe('JobProfileTemplateComponent', () => {
  let component: JobProfileTemplateComponent;
  let fixture: ComponentFixture<JobProfileTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobProfileTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobProfileTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
