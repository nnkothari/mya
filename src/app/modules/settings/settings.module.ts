import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';
import { AccountComponent } from './account/account.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { CompanyInfoComponent } from './company-info/company-info.component';
import { UsersComponent } from './users/users.component';
import { JobProfileTemplateComponent } from './job-profile-template/job-profile-template.component';
import { RosaComponent } from './rosa/rosa.component';


@NgModule({
  declarations: [SettingsComponent, AccountComponent, EmployeeDetailsComponent, NotificationsComponent, CompanyInfoComponent, UsersComponent, JobProfileTemplateComponent, RosaComponent],
  imports: [
    SharedModule,
    SettingsRoutingModule
  ]
})
export class SettingsModule { }
