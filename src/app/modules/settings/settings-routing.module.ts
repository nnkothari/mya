import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingsComponent } from './settings.component';
import { AccountComponent } from './account/account.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { CompanyInfoComponent } from './company-info/company-info.component';
import { UsersComponent } from './users/users.component';
import { JobProfileTemplateComponent } from './job-profile-template/job-profile-template.component';
import { RosaComponent } from './rosa/rosa.component';

const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
      {
        path: 'account',
        component: AccountComponent,
      },
      {
        path: 'employee-details',
        component: EmployeeDetailsComponent,
      },
      {
        path: 'notifications',
        component: NotificationsComponent,
      },
      {
        path: 'company-info',
        component: CompanyInfoComponent,
      },
      {
        path: 'users',
        component: UsersComponent,
      },
      {
        path: 'job-profile-template',
        component: JobProfileTemplateComponent,
      },
      {
        path: 'rosa',
        component: RosaComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
