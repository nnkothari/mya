import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

import { ToastrMessageService } from 'src/app/shared/services/toastr-message.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { Validation } from 'src/app/shared/validation';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  userRoleId: any;
  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrMessageService,
    private sharedService: SharedService
  ) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      userName: new FormControl('', [Validators.required,
      Validation.isValidMailFormat]),
      password: new FormControl('', [Validators.required])
    });
  }

  loginUser() {
    this.toastr.showMessage({ title: 'Error', text: 'Please fill all required details!!', type: 'error' });
    /* Open modal dialog. */
    this.sharedService.openModalPopup('confirmDialog', '');
    if (this.loginForm.invalid) {
      return;
    }
  }
}
