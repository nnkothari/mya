export interface BaseModel<T> {
    result: T;
    targetUrl: string | null;
    success: boolean;
    error: string | null;
    unAuthorizedRequest: boolean;
    __dsi: boolean;
}